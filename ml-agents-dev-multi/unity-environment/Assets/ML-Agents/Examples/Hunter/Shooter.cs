﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class Shooter : MonoBehaviour {

    public Transform hunter,
        gunTip;

    private Animator aiAnimator;

    private AICharacterControl aiController;

    private bool fireBullet;

    private float fireRange = 0.1f;

    void Awake() {
        aiAnimator = GetComponent<Animator>();

        aiController = GetComponent<AICharacterControl>();
        aiController.ToggleFiring(true);
    }

    void Update() {
        transform.LookAt(hunter);

        AnimatorStateInfo animationState = aiAnimator.GetCurrentAnimatorStateInfo(0);
        if (animationState.IsName("Shoot")) {
            float animationTime = animationState.normalizedTime % 1;
            if (animationTime < .1f && !fireBullet) {
                fireBullet = true;

                Vector3 shootDirection = new Vector3(
                    gunTip.forward.x + Random.Range(-fireRange, fireRange),
                    gunTip.forward.y + Random.Range(-fireRange, fireRange),
                    gunTip.forward.z + Random.Range(-fireRange, fireRange));

                RaycastHit hitInfo;
                if (Physics.Raycast(gunTip.position, shootDirection, out hitInfo, 20)) {
                    Debug.DrawLine(gunTip.position, hitInfo.point, Color.red);

                    if (hitInfo.transform.GetComponent<HunterAgent>()) {
                        hitInfo.transform.GetComponent<HunterAgent>().AgentShot();
                    }
                }

                Debug.DrawRay(gunTip.position, shootDirection, Color.green);
            }
            else if (animationTime > .9f && fireBullet) fireBullet = false;
        }
    }
}
