using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson {

    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour {

        public UnityEngine.AI.NavMeshAgent agent { get; private set; } 
        public ThirdPersonCharacter character { get; private set; } 
        public Transform target;

        public bool firing, reloading, dead;

        //x: horizontal move (-1 -> 1 range), y: rotation (-1 -> 1 range), z: vertical move (-1 -> 1 range)
        public Vector3 moveDelta;

        private bool targetMovement;

        void Start() {
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

            agent.updateRotation = false;
            agent.updatePosition = true;
        }

        private void Update() {
            if (firing || reloading) {
                //if target movement, stop to allow for firing or reloading
                if (targetMovement) {
                    agent.ResetPath();

                    character.Move(Vector3.zero, false, false);
                }

                character.Action(firing, reloading);

                //immediately revert reloading since it dosen't need to loop
                reloading = false;
            }
            else {
                //finish resolved actions
                character.Action(false, false);

                //if target, do target movement
                if (target != null) {
                    moveDelta = Vector3.zero;

                    targetMovement = true;

                    agent.SetDestination(target.position);

                    if (agent.remainingDistance > agent.stoppingDistance) character.Move(agent.desiredVelocity, false, false);
                    else character.Move(Vector3.zero, false, false);
                }
                else {
                    //if no more target, stop target movement
                    if (targetMovement) {
                        agent.ResetPath();

                        character.Move(Vector3.zero, false, false);

                        //if agent standing still, target movement complete
                        if (GetComponent<Rigidbody>().velocity.x == 0 &&
                            GetComponent<Rigidbody>().velocity.z == 0) {
                            targetMovement = false;
                        }
                    }
                    //do manual movement
                    else {
                        character.Move(moveDelta, false, false);
                    }
                }
            }
        }

        //target movement - set target for agent to path to
        public void MoveToTarget(Transform newTarget) {
            target = newTarget;
        }

        //vector3 movement - x: horizontal move (-1 -> 1 range), y: rotation (-1 -> 1 range), z: vertical move (-1 -> 1 range)
        public void MoveAgent(Vector3 newMoveDelta) {
            moveDelta = newMoveDelta;
        }

        //turn firing on/off
        public void ToggleFiring(bool toggle) {
            firing = toggle;
        }

        //reload
        public void Reload() {
            reloading = true;
        }

        public void ToggleDeath(bool toggle) {
            dead = toggle;

            character.Death(toggle);
        }
    }
}
